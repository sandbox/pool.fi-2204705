<?php

/**
 * @file
 * Adaptable payment entity editing UI.
 */

/**
 * Adaptable payment UI controller.
 */
class AdaptablePaymentUIController extends EntityBundleableUIController {
  /**
   * Provides definitions for implementing hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path . '/add']['title'] = 'Add adaptable payment';
    return $items;
  }
}

/**
 * Generates the adaptable payment entity form.
 */
function adaptable_payment_form($form, &$form_state, Entity $adaptable_payment, $op = 'edit') {

  // Needed by entity_form_field_validate().
  $form['type'] = array('#type' => 'value', '#value' => $adaptable_payment->type);

  // Form save/delete actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('adaptable_payment_form_submit_delete'),
    '#access' => ($op != 'add' && $op != 'clone') && adaptable_payment_access('delete', $adaptable_payment),
  );

  // Attach Field API fields.
  field_attach_form('adaptable_payment', $adaptable_payment, $form, $form_state);

  return $form;
}

/**
 * Form API validation validation callback for adaptable_payment_form().
 */
function adaptable_payment_form_validate(&$form, &$form_state) {
  entity_form_field_validate('adaptable_payment', $form, $form_state);
}

/**
 * Form API submit button callback for adaptable_payment_form().
 */
function adaptable_payment_form_submit(&$form, &$form_state) {
  $adaptable_payment = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go to entity.
  $adaptable_payment->save();
  $form_state['redirect'] = ADAPTABLE_PAYMENT_PATH . '/' . $adaptable_payment->pid;
}

/**
 * Form API delete button callback for adaptable_payment_form().
 */
function adaptable_payment_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = ADAPTABLE_PAYMENT_PATH . '/' . $form_state['adaptable_payment']->pid . '/delete';
}
