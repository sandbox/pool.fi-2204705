Introduction
-----------------------
Adaptable payments aims to assist site builders with charging for system events.
Some possible use cases are:

- Pay to publish.
- Paid subscriptions ("Memberships").
- User balance account.
...

None of this functionality is provided by the module itself. Instead, it is
recommended to use the Rules module to set up the behaviors according
to your own needs.

Configuration
-----------------------
When the module has been enabled, you need to create an Adaptable payment type
to continue. Adaptable payment types are fieldable, and group similar Adaptable
payments together. These types are managed from:

Administration > Configuration > Web services > Payment > Adaptable payment types

You will need to create at least one Adaptable payment type in order to use this
module. You will have to set up currency for the "line items" field, which is
provided by default.

Adding fields to your Adaptable payments types will make different use
cases possible. Using the use case example from the introduction, Adaptable payment
types and their additional fields might be:

- Subscription: Add a "duration" field of the type Interval to the
  Adaptable payment type and a date field "Expiration date" to
  the User.
- User balance: Add an integer field "balance" to the Adaptable payment
  and the user.
- Pay to publish: No additional fields needed.

When an Adaptable payment type has been created, the next step is to create
the available Adaptable payments for this type. This is done from:

Content > Adaptable payments > Add adaptable payment

Create the different payment options that you need and add their line items
and prices.

Setting up Rules
-----------------------
Although not a requirement, most use cases will need the Rules module to
provide desired functionality.

The Rules event "After finishing payment execution from an adaptable payment."
is what you will want to use. The example use case conditions will be:

- Subscription: 
  - Payment status is derived from: Completed.
  - Entity is of bundle: (Adaptable payment) Subscription.
- User balance:
  - Payment status is derived from: Completed.
  - Entity is of bundle: (Adaptable payment) User balance.
- Pay to publish:
  - Payment status is derived from: Completed.
  - Entity is of bundle: (Adaptable payment) Pay to publish.
  - Entity is of type: (Context entity) Node.

The actions for each payment type is as following:

- Subscription: 
  - Add an interval to date: Add "duration" to "Expiration date"
- User balance:
  - Calculate a value: Add Adaptable Payment and User balance together.
  - Set a data value: Set user balance to previously calculated value.
  - Save entity: User.
- Pay to publish:
  - Publish content: Context entity.

Context entities
-----------------------
Context entities are entities that are provided to Rules. Currently, the
only way to provide an entity is to display the Adaptable payment as an
Entity Views Attachment. For pay to publish to work, you will need to set
up a view with an EVA display on the bundle you wish to charge for.
