<?php

/**
 * @file
 * Provides Entity metadata integration.
 */

/**
 * Extend the defaults.
 */
class AdaptablePaymentMetadataController extends EntityDefaultMetadataController {

  /**
   * Define metadata properties.
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['type'] = array(
      'type' => 'adaptable_payment_type',
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer adaptable payments',
      'required' => TRUE,
      'description' => t('The adaptable payment type.'),
    ) + $properties['type'];

    $properties['created'] = array(
      'label' => t('Date created'),
      'type' => 'date',
      'description' => t('The date the adaptable payment was created.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer adaptable payments',
      'schema field' => 'created',
    );

    $properties['changed'] = array(
      'label' => t('Date changed'),
      'type' => 'date',
      'schema field' => 'changed',
      'description' => t('The date the adaptable payment was most recently updated.'),
    );

    return $info;
  }
}
