<?php
/**
 * @file
 * adaptable_payment_subscriptions.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function adaptable_payment_subscriptions_default_rules_configuration() {
  $items = array();
  $items['rules_adaptable_payment_pc_base_date'] = entity_import('rules_config', '{ "rules_adaptable_payment_pc_base_date" : {
      "LABEL" : "Get minimal base date ",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Adaptable payment content subscriptions" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "subscription_end_date" : { "label" : "Subscription base date", "type" : "date" } },
      "IF" : [
        { "data_is" : { "data" : [ "subscription-end-date" ], "op" : "\\u003C", "value" : "now" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "subscription-end-date" ], "value" : "now" } }
      ],
      "PROVIDES VARIABLES" : [ "subscription_end_date" ]
    }
  }');
  $items['rules_adaptable_payment_pc_current_user_default_date'] = entity_import('rules_config', '{ "rules_adaptable_payment_pc_current_user_default_date" : {
      "LABEL" : "Set current user default date",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Adaptable payment content subscriptions" ],
      "REQUIRES" : [ "rules" ],
      "IF" : [
        { "data_is_empty" : { "data" : [ "site:current-user:field-adaptable-payment-pc-acces" ] } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "site:current-user:field-adaptable-payment-pc-acces" ],
            "value" : "now"
          }
        }
      ]
    }
  }');
  $items['rules_adaptable_payment_pc_end'] = entity_import('rules_config', '{ "rules_adaptable_payment_pc_end" : {
      "LABEL" : "Update subscription end date",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Adaptable payment content subscriptions" ],
      "REQUIRES" : [ "rules", "payment", "interval", "adaptable_payment" ],
      "ON" : { "adaptable_payment_pre_finish" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "adaptable-payment" ],
            "type" : "adaptable_payment",
            "bundle" : { "value" : { "content_subscriptions" : "content_subscriptions" } }
          }
        },
        { "payment_rules_condition_payment_status_has_ancestor" : {
            "payment" : [ "payment" ],
            "payment_statuses" : { "value" : { "payment_status_money_transferred" : "payment_status_money_transferred" } }
          }
        }
      ],
      "DO" : [
        { "user_add_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3" } }
          }
        },
        { "component_rules_adaptable_payment_pc_current_user_default_date" : [] },
        { "component_rules_adaptable_payment_pc_base_date" : {
            "USING" : { "subscription_end_date" : [ "site:current-user:field-adaptable-payment-pc-acces" ] },
            "PROVIDE" : { "subscription_end_date" : { "subscription_end_date" : "Subscription base date" } }
          }
        },
        { "interval_apply" : {
            "USING" : {
              "interval" : [ "adaptable-payment:field-adaptable-payment-pc-durat:interval" ],
              "period" : [ "adaptable-payment:field-adaptable-payment-pc-durat:period" ],
              "date" : [ "subscription-end-date" ]
            },
            "PROVIDE" : { "date" : { "updated_end_date" : "New subscription end date" } }
          }
        },
        { "data_set" : {
            "data" : [ "site:current-user:field-adaptable-payment-pc-acces" ],
            "value" : [ "updated-end-date" ]
          }
        },
        { "entity_save" : { "data" : [ "site:current-user" ] } }
      ]
    }
  }');
  $items['rules_adaptable_payment_pc_expire'] = entity_import('rules_config', '{ "rules_adaptable_payment_pc_expire" : {
      "LABEL" : "Remove access for expired users",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Adaptable payment content subscriptions" ],
      "REQUIRES" : [ "adaptable_payment_subscriptions", "rules" ],
      "ON" : { "cron" : [] },
      "DO" : [
        { "adaptable_payment_subscriptions_load_expired_users" : { "PROVIDE" : { "expired_users" : { "expired_users" : "Expired users" } } } },
        { "LOOP" : {
            "USING" : { "list" : [ "expired-users" ] },
            "ITEM" : { "expired_user" : "Expired user" },
            "DO" : [
              { "component_rules_remove_subscriber_permission" : {
                  "USING" : { "user" : [ "expired-user" ] },
                  "PROVIDE" : { "user" : { "user" : "User" } }
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_remove_subscriber_permission'] = entity_import('rules_config', '{ "rules_remove_subscriber_permission" : {
      "LABEL" : "Remove subscriber permission",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Adaptable payment content subscriptions" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "user" : { "label" : "User", "type" : "user" } },
      "IF" : [
        { "user_has_role" : { "account" : [ "user" ], "roles" : { "value" : { "3" : "3" } } } }
      ],
      "DO" : [
        { "user_remove_role" : { "account" : [ "user" ], "roles" : { "value" : { "3" : "3" } } } },
        { "entity_save" : { "data" : [ "user" ] } }
      ],
      "PROVIDES VARIABLES" : [ "user" ]
    }
  }');
  return $items;
}
