<?php
/**
 * @file
 * adaptable_payment_subscriptions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function adaptable_payment_subscriptions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view any content_subscriptions adaptable payment'.
  $permissions['view any content_subscriptions adaptable payment'] = array(
    'name' => 'view any content_subscriptions adaptable payment',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'adaptable_payment',
  );

  return $permissions;
}
