<?php
/**
 * @file
 * adaptable_payment_subscriptions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function adaptable_payment_subscriptions_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'adaptable_payment_label'
  $field_bases['adaptable_payment_label'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'adaptable_payment',
    ),
    'field_name' => 'adaptable_payment_label',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 1,
    'max_length' => 255,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'adaptable_payment_line_items'
  $field_bases['adaptable_payment_line_items'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'adaptable_payment',
    ),
    'field_name' => 'adaptable_payment_line_items',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paymentform',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'paymentform',
  );

  // Exported field_base: 'field_adaptable_payment_pc_acces'
  $field_bases['field_adaptable_payment_pc_acces'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_adaptable_payment_pc_acces',
    'field_permissions' => array(
      'type' => 2,
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => '',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datestamp',
  );

  // Exported field_base: 'field_adaptable_payment_pc_body'
  $field_bases['field_adaptable_payment_pc_body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_adaptable_payment_pc_body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_adaptable_payment_pc_durat'
  $field_bases['field_adaptable_payment_pc_durat'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_adaptable_payment_pc_durat',
    'foreign keys' => array(),
    'indexes' => array(
      'interval' => array(
        0 => 'interval',
      ),
      'period' => array(
        0 => 'period',
      ),
    ),
    'locked' => 0,
    'module' => 'interval',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'interval',
  );

  return $field_bases;
}
