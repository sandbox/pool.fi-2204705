<?php
/**
 * @file
 * adaptable_payment_subscriptions.features.inc
 */

/**
 * Implements hook_default_adaptable_payment_type().
 */
function adaptable_payment_subscriptions_default_adaptable_payment_type() {
  $items = array();
  $items['content_subscriptions'] = entity_import('adaptable_payment_type', '{
    "type" : "content_subscriptions",
    "label" : "Content subscriptions",
    "description" : "Payment type for subscribing to the role \\u0022Content subscriber\\u0022.",
    "attached" : "0",
    "attached_bundles" : [],
    "data" : null
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function adaptable_payment_subscriptions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function adaptable_payment_subscriptions_node_info() {
  $items = array(
    'adaptable_payment_paid_content' => array(
      'name' => t('Premium content'),
      'base' => 'node_content',
      'description' => t('Content exclusive to subscribing users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
