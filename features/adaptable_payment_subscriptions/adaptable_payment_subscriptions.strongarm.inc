<?php
/**
 * @file
 * adaptable_payment_subscriptions.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function adaptable_payment_subscriptions_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_adaptable_payment_paid_content';
  $strongarm->value = array(
    'view_own' => array(
      0 => 3,
    ),
    'view' => array(
      0 => 3,
    ),
  );
  $export['content_access_adaptable_payment_paid_content'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__adaptable_payment_paid_content';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__adaptable_payment_paid_content'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_adaptable_payment_paid_content';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_adaptable_payment_paid_content'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_adaptable_payment_paid_content';
  $strongarm->value = '1';
  $export['node_preview_adaptable_payment_paid_content'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_adaptable_payment_paid_content';
  $strongarm->value = 1;
  $export['node_submitted_adaptable_payment_paid_content'] = $strongarm;

  return $export;
}
