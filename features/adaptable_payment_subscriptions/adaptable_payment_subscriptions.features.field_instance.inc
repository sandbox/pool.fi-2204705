<?php
/**
 * @file
 * adaptable_payment_subscriptions.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function adaptable_payment_subscriptions_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'adaptable_payment-content_subscriptions-adaptable_payment_label'
  $field_instances['adaptable_payment-content_subscriptions-adaptable_payment_label'] = array(
    'bundle' => 'content_subscriptions',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The human-readable name of this payment item',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => -5,
      ),
    ),
    'entity_type' => 'adaptable_payment',
    'field_name' => 'adaptable_payment_label',
    'label' => 'Label',
    'required' => TRUE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => -5,
    ),
  );

  // Exported field_instance: 'adaptable_payment-content_subscriptions-adaptable_payment_line_items'
  $field_instances['adaptable_payment-content_subscriptions-adaptable_payment_line_items'] = array(
    'bundle' => 'content_subscriptions',
    'deleted' => 0,
    'description' => 'Payment line items for this payment item.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paymentform',
        'settings' => array(),
        'type' => 'paymentform',
        'weight' => -4,
      ),
    ),
    'entity_type' => 'adaptable_payment',
    'field_name' => 'adaptable_payment_line_items',
    'label' => 'Line items',
    'required' => TRUE,
    'settings' => array(
      'currency_code' => 'XXX',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'paymentform',
      'settings' => array(),
      'type' => 'paymentform_line_item',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'adaptable_payment-content_subscriptions-field_adaptable_payment_pc_durat'
  $field_instances['adaptable_payment-content_subscriptions-field_adaptable_payment_pc_durat'] = array(
    'bundle' => 'content_subscriptions',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the duration this payment will prolong the user\'s subscription time.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'interval',
        'settings' => array(),
        'type' => 'interval_default',
        'weight' => -3,
      ),
    ),
    'entity_type' => 'adaptable_payment',
    'field_name' => 'field_adaptable_payment_pc_durat',
    'label' => 'Subscription duration',
    'required' => 0,
    'settings' => array(
      'allowed_periods' => array(
        'day' => 'day',
        'fortnight' => 0,
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'quarter' => 0,
        'second' => 0,
        'week' => 0,
        'year' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'interval',
      'settings' => array(),
      'type' => 'interval_default',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-adaptable_payment_paid_content-field_adaptable_payment_pc_body'
  $field_instances['node-adaptable_payment_paid_content-field_adaptable_payment_pc_body'] = array(
    'bundle' => 'adaptable_payment_paid_content',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_adaptable_payment_pc_body',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'user-user-field_adaptable_payment_pc_acces'
  $field_instances['user-user-field_adaptable_payment_pc_acces'] = array(
    'bundle' => 'user',
    'deleted' => 0,
    'description' => 'The date when the user\'s access to Premium content expires.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_adaptable_payment_pc_acces',
    'label' => 'Subscription end date',
    'required' => 0,
    'settings' => array(
      'default_value' => 'strtotime',
      'default_value2' => 'same',
      'default_value_code' => '-1 day',
      'default_value_code2' => '',
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Label');
  t('Line items');
  t('Payment line items for this payment item.');
  t('Select the duration this payment will prolong the user\'s subscription time.');
  t('Subscription duration');
  t('Subscription end date');
  t('The date when the user\'s access to Premium content expires.');
  t('The human-readable name of this payment item');

  return $field_instances;
}
