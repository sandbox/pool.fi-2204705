<?php
/**
 * @file
 * adaptable_payment_subscriptions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function adaptable_payment_subscriptions_user_default_roles() {
  $roles = array();

  // Exported role: content subscriber.
  $roles['content subscriber'] = array(
    'name' => 'content subscriber',
    'weight' => 2,
  );

  return $roles;
}
