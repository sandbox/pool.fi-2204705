<?php
/**
 * @file
 * Rules integration for the Adaptable payments module.
 */

/**
 * Implements hook_rules_event_info().
 */
function adaptable_payment_rules_event_info() {
  $event['adaptable_payment_status_change'] = array(
    'label' => t("After changing an adaptable payment's payment status"),
    'group' => t('Adaptable payment'),
    'access callback' => 'adaptable_payment_rules_access',
    'variables' => array(
      'adaptable_payment' => array(
        'type' => 'adaptable_payment',
        'label' => t('Adaptable payment'),
        'description' => t('The adaptable payment entity that caused the execution.'),
      ),
      'payment' => array(
        'type' => 'payment',
        'label' => t('Payment'),
      ),
      'context_entity' => array(
        'type' => 'entity',
        'label' => t('Context entity'),
        'description' => t('The entity this adaptable payment is attached to.'),
      ),
      'payment_old_status' => array(
        'type' => 'text',
        'label' => t("The payment's old status"),
      ),
    ),
  );
  $event['adaptable_payment_pre_execute'] = array(
    'label' => t('Before executing a payment from an adaptable payment entity.'),
    'group' => t('Adaptable payment'),
    'access callback' => 'adaptable_payment_rules_access',
    'variables' => array(
      'adaptable_payment' => array(
        'type' => 'adaptable_payment',
        'label' => t('Adaptable payment'),
        'description' => t('The adaptable payment entity that caused the execution.'),
      ),
      'payment' => array(
        'type' => 'payment',
        'label' => t('The Payment entity associated with the adaptable payment'),
      ),
      'context_entity' => array(
        'type' => 'entity',
        'label' => t('Context entity'),
        'description' => t('The entity this adaptable payment is attached to.'),
      ),
    ),
  );
  $event['adaptable_payment_pre_finish'] = array(
    'label' => t("When resuming the user's work after finishing an adaptable payment's payment execution."),
    'group' => t('Adaptable payment'),
    'access callback' => 'adaptable_payment_rules_access',
    'variables' => array(
      'adaptable_payment' => array(
        'type' => 'adaptable_payment',
        'label' => t('Adaptable payment'),
        'description' => t('The Adaptable payment entity that caused the execution.'),
      ),
      'payment' => array(
        'type' => 'payment',
        'label' => t('The Payment entity associated with the Adaptable payment'),
      ),
      'context_entity' => array(
        'type' => 'entity',
        'label' => t('Context entity'),
        'description' => t('An entity related to this adaptable payment. Currently only supports entities that uses adaptable payments as <a href="@eva-link">EVA</a> fields.', array('@eva-link' => 'https://drupal.org/project/eva')),
      ),
    ),
  );
  return $event;
}
