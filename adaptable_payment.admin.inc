<?php

/**
 * @file
 * Admin UI for Adaptable Payments.
 */

/**
 * Type UI controller.
 */
class AdaptablePaymentTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage adaptable payments and their respective fields.';
    return $items;
  }
}

/**
 * Generates the adaptable payment type editing form.
 */
function adaptable_payment_type_form($form, &$form_state, $adaptable_payment_type, $op = 'edit') {

  if ($op == 'clone') {
    $adaptable_payment_type->label .= ' (cloned)';
    $adaptable_payment_type->type = '';
  }

  // Save payment type to form state.
  $form_state['#adaptable_payment_type'] = $adaptable_payment_type;

  // Label field.
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $adaptable_payment_type->label,
    '#description' => t('The human-readable name of this adaptable payment type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($adaptable_payment_type->type) ? $adaptable_payment_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $adaptable_payment_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'adaptable_payment_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this adaptable payment type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  // Textual description for administrative purposes.
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => isset($adaptable_payment_type->description) ? $adaptable_payment_type->description : '',
    '#description' => t('Describe this adaptable payment type. The text will be displayed on the <em>Add new adaptable payment</em> page.'),
  );

  // Attach to entity types.
  $form['attach'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attached entities'),
    '#description' => t('By attaching the adaptable payment type to entities, the adaptable payment will become available as a field to that entity, and the entity will be exposed to Rules when a payment is executed.'),
  );

  // Attach toggle.
  $form['attach']['attached'] = array(
    '#title' => t('Attach to entities'),
    '#type' => 'checkbox',
    '#default_value' => isset($adaptable_payment_type->attached) ? $adaptable_payment_type->attached : 0,
    '#description' => t('Attach this adaptable payment to entity/entities.'),
  );
  $form['attach']['entity_selection'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'input[name="attached"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Load array of applicable entity types and bundles.
  $entities_info = entity_get_info();

  // Not possible to attach to adaptable payments.
  unset($entities_info['adaptable_payment']);

  $entity_options = array();
  foreach ($entities_info as $entity_type => $entity_info) {
    if (!empty($entity_info['view modes'])) {
      $bundle_options = array();
      foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
        $bundle_options[$bundle] = $bundle_info['label'];
      }
      if (count($bundle_options)) {
        $form['attach']['entity_selection'][$entity_type] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($entity_info['label']),
          '#collapsible' => TRUE,
          '#collapsed' => empty($adaptable_payment_type->attached_bundles[$entity_type]),
        );
        $form['attach']['entity_selection'][$entity_type][$entity_type] = array(
          '#type' => 'checkboxes',
          '#options' => $bundle_options,
          '#parents' => array('attached_bundles', $entity_type),
          '#default_value' => isset($adaptable_payment_type->attached_bundles[$entity_type]) ? $adaptable_payment_type->attached_bundles[$entity_type] : array(),
        );
      }
    }
  }

  $form['data']['#tree'] = TRUE;
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save adaptable payment type'),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete adaptable payment type'),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('adaptable_payment_type_form_submit_delete'),
    '#access' => !$adaptable_payment_type->isLocked() && $op != 'add' && $op != 'clone',
  );
  return $form;
}

/**
 * Form API validation callback for the type form.
 */
function adaptable_payment_type_form_validate(&$form, &$form_state) {
  $attached_bundles = array();
  $adaptable_payment_type = $form_state['#adaptable_payment_type'];
  foreach ($form_state['values']['attached_bundles'] as $entity_type => $bundles) {
    // Clean up selected bundles.
    $bundles = array_filter($bundles);
    // Add bundles to result.
    if (count($bundles)) {
      $attached_bundles[$entity_type] = $bundles;
    }
  }
  // Overwrite form state with cleaned data.
  $form_state['values']['attached_bundles'] = $attached_bundles;
}

/**
 * Form API submit callback for the type form.
 */
function adaptable_payment_type_form_submit(&$form, &$form_state) {
  $adaptable_payment_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Add default fields to bundle.
  adaptable_payment_type_add_default_fields($adaptable_payment_type->type);
  // Save and go back.
  $adaptable_payment_type->save();
  // Clear field cache for forms to appear on selected entities.
  field_cache_clear();
  $form_state['redirect'] = ADAPTABLE_PAYMENT_TYPE_PATH;
}

/**
 * Form API submit callback for the delete button.
 */
function adaptable_payment_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = ADAPTABLE_PAYMENT_TYPE_PATH;
}

/**
 * Adds default fields to an adaptable payment type.
 *
 * @param string $bundle
 *   The adaptable payment type machine name.
 */
function adaptable_payment_type_add_default_fields($bundle) {
  // Information about the fields that are about to be added.
  $fields = array(
    ADAPTABLE_PAYMENT_LABEL_FIELD => array(
      'type' => 'text',
      'widget' => 'text_textfield',
      'label' => t('Label'),
      'description' => t('The human-readable name of this payment item'),
    ),
    ADAPTABLE_PAYMENT_LINE_ITEMS_FIELD => array(
      'type' => 'paymentform',
      'widget' => 'paymentform_line_item',
      'label' => t('Line items'),
      'description' => t('Payment line items for this payment item.'),
    ),
  );
  // Create field instances for this bundle.
  $weight = -5;
  foreach ($fields as $field_name => $field_info) {
    if (!field_info_instance('adaptable_payment', $field_name, $bundle)) {
      $instance = array(
        'field_name' => $field_name,
        'bundle' => $bundle,
        'widget' => array(
          'type' => $field_info['widget'],
          'weight' => $weight,
        ),
        'display' => array(
          'default' => array(
            'weight' => $weight,
          ),
        ),
        'entity_type' => 'adaptable_payment',
        'label' => $field_info['label'],
        'description' => $field_info['description'],
        'required' => TRUE,
      );
      field_create_instance($instance);
    }
    ++$weight;
  }
}
