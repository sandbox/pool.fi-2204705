<?php

/**
 * @file
 * Holds entity related classes.
 */

/**
 * The class used for adaptable payment entities.
 */
class AdaptablePayment extends Entity {

  public $created;
  public $changed;
  public $pid;
  public $type;
  public $uid;
  public $label;

  /**
   * Add type property in constructor.
   */
  public function __construct($values = array()) {
    if (isset($values['type']) && is_object($values['type'])) {
      $values['type'] = $values['type']->type;
    }
    parent::__construct($values, 'adaptable_payment');
  }

  /**
   * Add timestamps when saving.
   */
  public function save() {
    $op = 'update';

    // Set created and changed timestamps.
    if (empty($this->created) && (!empty($this->is_new) || !$this->pid)) {
      $op = 'insert';
      $this->created = REQUEST_TIME;
    }
    $this->changed = REQUEST_TIME;

    parent::save();

    // Create pathalias.
    if (module_exists('pathauto')) {
      $source = $this->defaultUri();
      module_load_include('inc', 'pathauto');
      pathauto_create_alias('adaptable_payment', $op, $source['path'], array('adaptable_payment' => $this), $this->type);
    }
  }

  /**
   * Gets the associated adaptable payment type object.
   *
   * @return AdaptablePaymentType
   *   The AdaptablePaymentType object.
   */
  public function type() {
    return adaptable_payment_get_types($this->type);
  }

  /**
   * Implements a custom label callback.
   */
  public function label() {
    if (!isset($this->label)) {
      $entity_wrapper = entity_metadata_wrapper('adaptable_payment', $this);
      $this->label = $entity_wrapper->{ADAPTABLE_PAYMENT_LABEL_FIELD}->value();
    }
    return $this->label;
  }

  /**
   * Implements a custom default URI.
   */
  public function defaultUri() {
    return array(
      'path' => ADAPTABLE_PAYMENT_PATH . '/' . $this->pid,
    );
  }

  /**
   * Sets a new user.
   *
   * @param object $account
   *   The user account object or the user account id (uid).
   */
  public function setUser($account) {
    $this->uid = is_object($account) ? $account->uid : $account;
  }
}

/**
 * The type class of adaptable payment entities.
 */
class AdaptablePaymentType extends Entity {

  public $type;
  public $label;

  /**
   * Returns whether the adaptable payment type is locked.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
